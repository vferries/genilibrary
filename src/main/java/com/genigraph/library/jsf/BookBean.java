package com.genigraph.library.jsf;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Data;

import com.genigraph.library.model.Book;
import com.genigraph.library.rest.BookService;

@Data
@Named
@SessionScoped
public class BookBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private BookService service;
	private Book book = new Book();

	public String save() {
		service.create(book);
		book = new Book();
		return null;
	}
	
	public List<Book> getBooks() {
		return service.getAll();
	}
}
