package com.genigraph.library.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
public class Book {
	@Id
	@GeneratedValue
	private Long id;
	private String title;
	@Pattern(regexp = "[a-zA-Z][a-zA-Z-. ]*", message = "The name must start with a letter.")
	private String author;
	private String url;
	private String img;
	private String editor;
	private boolean available;
}
