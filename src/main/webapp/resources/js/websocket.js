var username;
function join() {
    username = textField.value;
    websocket.send(username + " joined");
}

function send_message() {
    websocket.send(username + ": " + textField.value);
}

function writeToScreen(message) {
    output.innerHTML += message + "<br>";
}


var wsUri = "ws://localhost:8080/library/ws";
var websocket = new WebSocket(wsUri);
websocket.onopen = function(evt) {
    writeToScreen("Connected to " + wsUri);
};
websocket.onmessage = function(evt) {
    console.log("onMessage");
    writeToScreen("RECEIVED: " + evt.data);
    if (evt.data.indexOf("joined") != -1) {
        userField.innerHTML += evt.data.substring(0, evt.data.indexOf(" joined")) + "\n";
    } else {
        chatlogField.innerHTML += evt.data + "\n";
    }
};
websocket.onerror = function(evt) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
};

var output = document.getElementById("output");
